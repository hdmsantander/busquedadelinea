# Ejemplo de aplicación web usando R y Shiny
### Una implementación de un algoritmo de búsqueda de línea

El presente repositorio ejemplifica el uso de Shiny en Rstudio para implementar una aplicación web responsiva que sirve de front end para un algoritmo de búsqueda para una función de varias variables.

Se puede ver el código en ejemplo y su implementación en línea en [MatrixDS](https://community.platform.matrixds.com/hdmsantander/p5dc4b5f29550a31e).